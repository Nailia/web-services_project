﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for RecieptsList.xaml
    /// </summary>
    public partial class RecieptsList : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public RecieptsList()
        {
            InitializeComponent();
            ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getAllReceiptsAsStrings() }) ;
               
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string firstName = txtSearch.Text;
            ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getReceiptByCustomerFirsName(firstName) });
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowView = ViewReceiptsDataGridView.SelectedItem as DataRowView;
            string receiptID = rowView.Row[0].ToString();
            int id = int.Parse(receiptID);
            if (MessageBox.Show("You Realy want to delete number " + receiptID+ " Receipt information?", "Message", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {                
                if(objWebservice.deleteReceipt(id) == 1)
                {
                    MessageBox.Show("Receipt deleted successfully");
                    ViewReceiptsDataGridView.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getAllReceiptsAsStrings() });
                }
                else
                {
                    MessageBox.Show("You could not delete.There are some issue.");
                }                
            }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowView = ViewReceiptsDataGridView.SelectedItem as DataRowView;
            string receiptID = rowView.Row[0].ToString();
            int Id = int.Parse(receiptID);
            string firstname = rowView.Row[1].ToString();
            string lastName = rowView.Row[2].ToString();
            string serviceName = rowView.Row[3].ToString();
            string price = rowView.Row[4].ToString();
            string MedicalIdNumber = rowView.Row[5].ToString();
            DateTime date =DateTime.Parse(rowView.Row[6].ToString());
            MessageBox.Show("Id " + Id+ "firstname " + firstname + "lastName " + lastName + "serviceName " + serviceName + "price " + price + "MedicalIdNumber " + MedicalIdNumber + "date " + date);
            ModifyReceipt mr = new ModifyReceipt(Id, firstname, lastName, serviceName, price,  MedicalIdNumber, date);
            mr.Show();  // or nc.Showdialog();
            this.Hide();

        }
    }
}
