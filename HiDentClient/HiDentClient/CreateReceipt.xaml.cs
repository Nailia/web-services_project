﻿using HiDentClient.localhost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for CreateReceipt.xaml
    /// </summary
    /// 
    

    public partial class CreateReceipt : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public CreateReceipt()
        {
            InitializeComponent();
            //Doctor comboBox
            List<ComboData> doctorData = new List<ComboData>(objWebservice.getDoctorsList());
            cbDoctorNumber.ItemsSource = doctorData;
            cbDoctorNumber.DisplayMemberPath = "Value";
            cbDoctorNumber.SelectedValuePath = "Id";            
            cbDoctorNumber.SelectedIndex = 0;
            
            //Service comboBox
            List<ComboData> serviceData = new List<ComboData>(objWebservice.getAllServices());
            cbServices.ItemsSource = serviceData;
            cbServices.DisplayMemberPath = "Value";
            cbServices.SelectedValuePath = "Id";
            cbServices.SelectedIndex = 0;
            
        }

        private void btnCreateReceipt_Click(object sender, RoutedEventArgs e)
        {
            string firstName = txtCustFirstName.Text;
            string lastName = txtCustLastName.Text;
            //check for customer name
            if (objWebservice.checkCustomer(firstName, lastName) == "")
            {
                MessageBox.Show("You don't have a customer under name of " + firstName + " " + lastName);
            }
            else if (!Regex.IsMatch(txtCustFirstName.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtCustLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name and Last Name must contain only letters");       
                int custID = int.Parse(objWebservice.checkCustomer(firstName, lastName));
                //check for Doctor Number comboBox
                if (cbDoctorNumber.SelectedIndex == 0)
                {
                    MessageBox.Show("Please choose a doctor");
                }
                else
                {
                    int doctorID = int.Parse(cbDoctorNumber.SelectedValue.ToString());
                    //check for Service comboBox
                    if (cbServices.SelectedIndex == 0)
                    {
                        MessageBox.Show("Please choose a service");
                    }
                    else
                    {
                        int serviceID = int.Parse(cbServices.SelectedValue.ToString());
                        //check for date
                        if (dpReceiptDate.SelectedDate == null)
                        {
                            MessageBox.Show("Please choose a date");
                        }
                        else
                        {
                            DateTime date = DateTime.Parse(dpReceiptDate.SelectedDate.ToString());
                            if (objWebservice.saveReceipt(doctorID, custID, serviceID, date) > 0)
                            {
                                MessageBox.Show("New receipt was created successfully");
                                new MainWindow().Show();
                            }
                            else
                            {
                                MessageBox.Show("New receipt wasn't created successfully");
                            }
                        }
                    }
                    

                }

            }   
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ServiceID = int.Parse(cbServices.SelectedValue.ToString());
            txtAmount.Text = objWebservice.getServicePriceById(ServiceID);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void cbDoctorNumber_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
