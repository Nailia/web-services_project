﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for FindCustomer.xaml
    /// </summary>
    public partial class FindCustomer : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();

        public FindCustomer()
        {
            InitializeComponent();
            dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getCustomersList() });
        }

       
        private void txtSearchClient_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.searchCustomer(txtSearchClient.Text) });

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowview = dgCustomers.SelectedItem as DataRowView;
            int id = int.Parse(rowview.Row[0].ToString());

            if (MessageBox.Show("Are you Sure you want to Delete " + rowview.Row[1].ToString() + " " + rowview.Row[2].ToString() + "?",
                                         "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                int j = objWebservice.deleteCustomer(id);
                if (j > 0)
                {
                    MessageBox.Show("Record Deleted Successfully!");
                    dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getCustomersList() });
                }
                this.Show();
            }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowview = dgCustomers.SelectedItem as DataRowView;
            int id = int.Parse(rowview.Row[0].ToString());
            string firstName = rowview.Row[1].ToString();
            string lastName = rowview.Row[2].ToString();
            string address = rowview.Row[3].ToString();
            string city = rowview.Row[4].ToString();
            string postalCode = rowview.Row[5].ToString();
            string country = rowview.Row[6].ToString();
            string phone = rowview.Row[7].ToString();
            string email = rowview.Row[8].ToString();
            ModifyCustomer nc = new ModifyCustomer(id, firstName, lastName, address, city, postalCode, country, phone, email);
            nc.Show();
            this.Hide();
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miDeleteClient_Click(object sender, RoutedEventArgs e)
        {
            DeleteCustomer nc = new DeleteCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

    }
}

