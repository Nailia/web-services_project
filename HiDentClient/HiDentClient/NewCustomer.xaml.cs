﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for NewCustomer.xaml
    /// </summary>
    public partial class NewCustomer : Window
    {
        //creating the object of web service class  
        localhost.WebService objWebservice = new localhost.WebService();

        string firstName, lastName, address, city, postalCode, country, phone, email;

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
        }

        public NewCustomer()
        {
            InitializeComponent();
        }

        private void btnAddClient_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //checking if required fields are empty or not
            if (txtFirstName.Text == "" || txtLastName.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("First name and Last name are required");
                //string required = "First name required";
                //lblFNameRequired.Content = required;
            }
            //First Name and Last Name must be contain only letters
            else if (!Regex.IsMatch(txtFirstName.Text, @"^[\p{L}]+$")
                    || !Regex.IsMatch(txtLastName.Text, @"^[\p{L}]+$"))
            {
                MessageBox.Show("First Name and Last Name must contain only letters");
            }
            // Email address must be valid one
            else if (txtEmail.Text != "" & !regex.IsMatch(txtEmail.Text))
            {
                MessageBox.Show("Please enter a valid email address");
            }
            //Phone number must be in required format
            else if (txtPhone.Text != "" & !Regex.IsMatch(txtPhone.Text, "\\p{N}{3}-\\p{N}{3}-\\p{N}{4}\\b"))
            {
                MessageBox.Show("Phone number format is:\n 555-555-5555");
            } else
            {
                firstName = txtFirstName.Text;
                lastName = txtLastName.Text;
                address = txtAddress.Text;
                city = txtCity.Text;
                postalCode = txtPostalCode.Text;
                country = txtCountry.Text;
                phone = txtPhone.Text;
                email = txtEmail.Text;

                // Check this customer for his existance
                string j = objWebservice.checkCustomer(firstName, lastName);

                if (j == "")
                {
                    // Insert this new customer
                    int i = objWebservice.AddNewCustomer(firstName, lastName, address, city, postalCode, country, phone, email);

                    if (i != 0)
                    {
                        if (MessageBox.Show("Customer information was saved successfully.\n" +
                            "Do you want to add another Customer?",
                            "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            //this.Show();
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtAddress.Text = "";
                            txtCity.Text = "";
                            txtPostalCode.Text = "";
                            txtCountry.Text = "";
                            txtPhone.Text = "";
                            txtEmail.Text = "";
                        }
                        else
                        {
                            this.Hide();
                            new MainWindow().Show();
                        }
                    }
                    
                }
                else //Customer already in DB
                {
                    MessageBox.Show("This client is already registered");
                }
            }
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miDeleteClient_Click(object sender, RoutedEventArgs e)
        {
            DeleteCustomer nc = new DeleteCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

    }//end partial class
}//end namespace
    

