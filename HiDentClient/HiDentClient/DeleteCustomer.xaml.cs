﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for DeleteCustomer.xaml
    /// </summary>
    public partial class DeleteCustomer : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();

        public DeleteCustomer()
        {
            InitializeComponent();
            dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getCustomersList() });
        }

        private void txtSearchClient_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.searchCustomer(txtSearchClient.Text) });
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowview = dgCustomers.SelectedItem as DataRowView;
            int id = int.Parse(rowview.Row[0].ToString());

            if (MessageBox.Show("Are you Sure you want to Delete " + rowview.Row[1].ToString() + " " + rowview.Row[2].ToString() + "?",
                                         "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                int j = objWebservice.deleteCustomer(id);
                if (j > 0)
                {
                    MessageBox.Show("Record Deleted Successfully!");
                    dgCustomers.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.getCustomersList() });
                }
                this.Show();
            }

        }
    }
}
