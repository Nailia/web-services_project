﻿using HiDentClient.localhost;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyReceipt.xaml
    /// </summary>
    public partial class ModifyReceipt : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public ModifyReceipt(int Id, String firstname, String lastName, String serviceName, String price, String MedicalIdNumber, DateTime date)
        {
            InitializeComponent();

            double amount = double.Parse(price);
            txtReceiptNumber.Text = Id.ToString();
            //Doctor comboBox
            List<ComboData> doctorData = new List<ComboData>(objWebservice.getDoctorsList());
            cbDoctorNumber.ItemsSource = doctorData;
            cbDoctorNumber.DisplayMemberPath = "Value";
            cbDoctorNumber.SelectedValuePath = "Id";
            cbDoctorNumber.Text = MedicalIdNumber;

            txtCustFirstName.Text = firstname;
            txtCustLastName.Text = lastName;

            //services comboBox
            List<ComboData> comboData = new List<ComboData>(objWebservice.getAllServices());
            comboData.AddRange(objWebservice.getAllServices());
            cbServices.ItemsSource = comboData;
            cbServices.DisplayMemberPath = "Value";
            cbServices.SelectedValuePath = "Id";
            cbServices.Text = serviceName;

            txtAmount.Text = amount.ToString("C", CultureInfo.CurrentCulture);
            dtpDate.SelectedDate = date; 

        }

       

        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            int receiptNumber = int.Parse(txtReceiptNumber.Text);
            string firstName = txtCustFirstName.Text;
            string lastName = txtCustLastName.Text;
            //check for customer name
            if(firstName == "" || lastName == "")
            {
                MessageBox.Show("First name and Lact name are required ");
            }
            else if (objWebservice.checkCustomer(firstName, lastName) == "")
            {
                MessageBox.Show("You don't have a customer under name of " + firstName + " " + lastName);
            }
            else 
            {                
                int custID = int.Parse(objWebservice.checkCustomer(firstName, lastName));
                //check for Doctor Number comboBox
                if (cbDoctorNumber.SelectedIndex == 0)
                {
                    MessageBox.Show("Please choose a doctor");
                }
                else
                {
                    int doctorID = int.Parse(cbDoctorNumber.SelectedValue.ToString());
                    //check for Service comboBox
                    if (cbServices.SelectedIndex == 0)
                    {
                        MessageBox.Show("Please choose a service");
                    }
                    else
                    {
                        int serviceID = int.Parse(cbServices.SelectedValue.ToString());
                        //check for date
                        if (dtpDate.SelectedDate == null)
                        {
                            MessageBox.Show("Please choose a date");
                        }
                        else
                        {
                            DateTime date = DateTime.Parse(dtpDate.SelectedDate.ToString());
                            if (objWebservice.updateReceipt(receiptNumber,doctorID, custID, serviceID, date) > 0)
                            {
                                MessageBox.Show("Your receipt was modified and saved successfully");
                                new RecieptsList().Show();
                                this.Hide();
                            }
                            else
                            {
                                MessageBox.Show("Your receipt was not modified there are some issues!!!");
                            }
                        }
                    }
                }
            }           
        }

        private void cbServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ServiceID = int.Parse(cbServices.SelectedValue.ToString());
            txtAmount.Text = objWebservice.getServicePriceById(ServiceID);
        }
    }
}
