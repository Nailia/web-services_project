﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace HiDentServer
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        SqlCommand cmd;
        SqlConnection con;
       
        public class ComboData
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }

        /*Nailia*/
        [WebMethod]
        public string checkCustomer(string firstName, string lastName)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string j = "";

            //check if the new customer already exist
            string qryCheck = "select CustID from Customer where CustFirstName = '" + firstName + "' and CustLastName = '" + lastName + "'";
            using (SqlCommand cmd = new SqlCommand(qryCheck, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var custID = reader["CustID"];
                    j = custID.ToString();
                }
            }
            return j;
        }

        [WebMethod]
        public int AddNewCustomer(string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Customer (CustFirstName, CustLastName, Address, City, ZipCode, Country, Phone, EmailAddress) " +
            "VALUES (@firstName, @lastName, @address, @city, @postalCode, @country, @phone, @email)", con);
            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@lastName", lastName);
            cmd.Parameters.AddWithValue("@address", address);
            cmd.Parameters.AddWithValue("@city", city);
            cmd.Parameters.AddWithValue("@postalCode", postalCode);
            cmd.Parameters.AddWithValue("@country", country);
            cmd.Parameters.AddWithValue("@phone", phone);
            cmd.Parameters.AddWithValue("@email", email);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        [WebMethod]
        public DataTable getCustomersList()
        {
            DataTable dtCustomers = new DataTable();
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Customer";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtCustomers.Load(reader);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtCustomers);
            return ds.Tables[0];
        }

        [WebMethod]
        public int deleteCustomer(int Id)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            // SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE from Customer WHERE (CustID='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }

        [WebMethod]
        public int editCustomer(int Id, string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Customer SET CustFirstName ='" + firstName + "', CustLastName ='" + lastName + "', Address ='" + address + "', City ='" + city + "', ZipCode ='" + postalCode + "', Country ='" + country + "', Phone ='" + phone + "', EmailAddress ='" + email + "' WHERE (CustID='" + Id + "')", con);
            var j = cmd.ExecuteNonQuery();
            con.Close();

            return j;
        }


        [WebMethod]
        public DataTable searchCustomer(string searchStr)
        {
            DataTable dtCustomers = new DataTable();
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            string query = "SELECT * FROM Customer where CustFirstName like '" + searchStr + "%'";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtCustomers.Load(reader);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtCustomers);
            return ds.Tables[0];
        }

        /*Shushan*/ 
        
        /// Receipts///      
        [WebMethod]
        public DataTable getAllReceiptsAsStrings()
        {            
            DataTable dtReceipt = new DataTable();
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            string qry = "select R.ReceiptNumber, C.CustFirstName as FirstName," +
                " C.CustLastName as LastName, S.serviceName as service, " +
                "S.Price as price,  D.MedicalIdNumber, R.Date from Receipt as R" +
                " inner join Customer as C on C.CustID=R.CustID " +
                "inner join Services as S on S.Id = R.ServiceID " +
                "inner join Doctors as D on D.Id=DoctorID";
            using (SqlCommand cmd = new SqlCommand(qry, con))
            {                
                SqlDataReader reader = cmd.ExecuteReader();
                dtReceipt.Load(reader);
            }
           
            DataSet ds = new DataSet();
            ds.Tables.Add(dtReceipt);
            return ds.Tables[0];
            
        }

        [WebMethod]
        public DataTable getReceiptByCustomerFirsName(string name)
        {
            DataTable dtReceipt = new DataTable();
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            string qry = "select R.ReceiptNumber, C.CustFirstName as firstName, " +
                " C.CustLastName as lastName, S.serviceName as service, " +
                "S.Price as price,  D.MedicalIdNumber, R.Date from Receipt as R" +
                " inner join Customer as C on C.CustID=R.CustID " +
                "inner join Services as S on S.Id = R.ServiceID " +
                "inner join Doctors as D on D.Id=DoctorID where C.CustFirstName like '" + name + "%'";

            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                dtReceipt.Load(reader);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtReceipt);
            return ds.Tables[0];
        }       

        [WebMethod]
        public int deleteReceipt(int Id)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("Delete from Receipt where ReceiptNumber='" + Id + "'", con);
            int i = cmd.ExecuteNonQuery();
            return i;
        }

        [WebMethod]
        public int saveReceipt(int DoctorID, int CustID, int ServiceID, DateTime Date)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            cmd = new SqlCommand("INSERT INTO Receipt (DoctorID, CustID, ServiceID, Date) " +
                "VALUES (@doctorID, @custID, @serviceID, @date)", con);
            cmd.Parameters.AddWithValue("@doctorID", DoctorID);
            cmd.Parameters.AddWithValue("@custID", CustID);
            cmd.Parameters.AddWithValue("@serviceID", ServiceID);
            cmd.Parameters.AddWithValue("@date", Date);

            int i = cmd.ExecuteNonQuery();
            return i;

        }

        [WebMethod]
        public int updateReceipt(int ReceiptNumber, int DoctorID, int CustID, int ServiceID, DateTime Date)
        {
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\LocalDB;Initial Catalog=HiDent;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Receipt SET DoctorID ='" + DoctorID + "', CustID ='" + CustID + "', ServiceID ='" + ServiceID + "', Date ='" + Date + "' WHERE (ReceiptNumber='" + ReceiptNumber + "')", con);
            int i = cmd.ExecuteNonQuery();
            con.Close();

            return i;
        }

        ///Serives///        
        [WebMethod]
        public List<ComboData> getAllServices()
        {
            List<ComboData> ListServiceData = new List<ComboData>();
            ListServiceData.Add(new ComboData { Id = 0, Value = "Select a service" });
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            SqlCommand cmd = new SqlCommand("Select Id, serviceName from Services", con);
            var reader = cmd.ExecuteReader();
            while(reader.Read()){
                ListServiceData.Add(new ComboData { Id = int.Parse(reader["Id"].ToString()), Value = reader["serviceName"].ToString() });             
            }
            con.Close();
            return ListServiceData;
        }

        [WebMethod]
        public string getServicePriceById(int Id)
        {
            string price = "";
            double amount;
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            string qry = "Select Price from Services where Id='"+Id+"'";

            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    amount = double.Parse(reader["Price"].ToString());
                    price = amount.ToString("C", CultureInfo.CurrentCulture);//ToFix: money format
                }
            }
            return price;
        }

        ///Doctor///
        [WebMethod]
        public List<ComboData> getDoctorsList()
        {
            List<ComboData> ListDoctorData = new List<ComboData>();
            ListDoctorData.Add(new ComboData { Id = 0, Value = "Select Doctor Number" });
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            
            string qry = "Select Id, MedicalIdNumber from Doctors";
            SqlCommand cmd = new SqlCommand(qry, con);
            
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListDoctorData.Add(new ComboData { Id = int.Parse(reader["Id"].ToString()), Value = reader["MedicalIdNumber"].ToString() });
            }
            con.Close();
        return ListDoctorData;
        }

        [WebMethod]
        public int getDoctorIdByMedicalIdNumber(string MedicalIdNumber)
        {            
            SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=HiDent;Integrated Security=True;Pooling=False");
            con.Open();
            int Id = 0;
            string qry = "Select Id from Doctors where MedicalIdNumber='" + MedicalIdNumber + "'";
            using (SqlCommand cmd = new SqlCommand(qry, con))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                     Id = int.Parse(reader["Id"].ToString());                  
                }                
            }
            return Id;
        }        
    }
}
